$(document).ready(function(){
    owlCorusel();
    busketDropDown();
    hiddenCarousel();
    languageSelector();
    dateToday();
});

const owlCorusel = function(){
    $("#icon_container").owlCarousel({
        dots: false,
        loop: false,
        nav: false,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
        responsiveClass:true,
        responsive:{
            320:{
                items:1,
                mouseDrag: true,
                touchDrag: true,
                pullDrag: true,
                loop: true,
                autoplay: true
            },
            768:{
                items:2,
                mouseDrag: true,
                touchDrag: true,
                pullDrag: true,
                loop: true,
                autoplay: true
            },
            1024:{
                items: 3
            }
        }});
    $("#slider_container").owlCarousel({
        items: 1,
        dots: false,
        loop: true,
        nav: true,
        navText: ["<i class=\"fas fa-chevron-left\"></i>","<i class=\"fas fa-chevron-right\"></i>"],
        margin:20
    });
    $(".images.owl-carousel").owlCarousel({
        items: 5,
        dots: false,
        loop: true,
        nav: true,
        navText: ["<i class=\"fas fa-angle-left\"></i>","<i class=\"fas fa-angle-right\"></i>"],
        margin:20,
        autoplay: true,
        autoplayTimeout: 3000,
        responsive:{
            320:{
                items: 1
            },
            768:{
                items: 3
            },
            1024:{
                items: 5
            }
    }});
};

const busketDropDown = function () {
    const basketForm = $('.basket-hidden');
    $('#basket-form').on('click', function(e){
        e.preventDefault();
        basketForm.toggleClass('active');
    });
    $(document).on('click', function (e) {
        if ($(e.target).closest('#basket-form').length === 0
         && $(e.target).closest('.basket-form').length === 0) {
            basketForm.removeClass('active');
        }
    })
};


const hiddenCarousel = function () {
    $('.section-2-li a').on('click', function (e) {
        e.preventDefault();
        const href = $(e.target).attr('href');
        $('.section-2-li .active').removeClass('active');
        $(e.target).addClass('active');
        $('.carousel.active').removeClass('active');
        $(href).addClass('active');
    })
};

const languageSelector = function () {
    $('.dropdown-menu').on('click', function (e) {
        e.preventDefault();
        $('.dropdown-menu').toggleClass('open');
    });
    $(document).on('click', function (e) {
        if($(e.target).closest('.dropdown-menu').length === 0){
            $('.dropdown-menu').removeClass('open');
        }
    });
    $('.dropdown-menu a').on('click', function (e) {
        const dataLang = $(e.target).attr('data-lang');
        const langText = $(e.target).text();
        $('.dropdown-menu .title').attr('data-lang', dataLang).text(langText);
        $('.dropdown-menu .disable').removeClass('disable');
        $(e.target).parent().addClass('disable');
    });
};


const dateToday = function () {
    var day = new Date();
    var hour = day.getHours();
    if (hour >= 9 && hour < 18) {
        document.getElementById("contArea").innerHTML =
            '<p>Call until 18:00</p> <img src="img/Ellipse%204.png" alt="phone" /><a href="tel:+780356236626"><span>035 - 623 66 26</span></a>';
    } else {
        document.getElementById("contArea").innerHTML =
            '<p>Call until 18:00</p> <img src="img/Ellipse%204-1.png" alt="e-mail" /><a href="mailto:simpleme@gmail.com?subject=Subject"><span>simpleme@gmail.com</span></a>';
    }
};







